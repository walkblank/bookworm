import requests
import sys, io
from bs4 import BeautifulSoup

BASE_URL = 'http://www.biquge.com.tw'

# desc   : get catelist of a book , include cate name and url 
# paras  :
#   @bookCageUrl  [string],   url of cagelog page of the book
# retval : [list] ,  list of book catelog  

def getBookCatelog(bookCateUrl): 
    #get catelog page html of a book
    bookPage = requests.get(bookCateUrl)
    #set html encode (get from page html meta content)
    bookPage.encoding = 'gbk'
    #make soup with html.parser
    bookPageSoup = BeautifulSoup(bookPage.text, 'html.parser')
    cateDiv = bookPageSoup.find_all('div', id='list')
    cateList = cateDiv[0].find_all('a')
    # print(lcon)
    # print(type(cateList[0]))
    # print(str(cateList))
    return cateList 

def getCateContent(cateUrl):
    catePage = requests.get(cateUrl)
    catePage.encoding = 'gbk' 
    catePageSoup = BeautifulSoup(catePage.text, 'html.parser')
    catePageContent = catePageSoup.find_all('div', id='content')
    # print(catePageContent)
    return catePageContent
    


if __name__ == '__main__':
    if(sys.platform == 'darwin'):
        #mac termnial defautl output encode is utf8
        sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='utf8')
    else:
        sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='gb18030')
    # create a file to store the book
    f = open('a.txt', 'w+', encoding='utf-8')
    cateList = getBookCatelog('http://www.biquge.com.tw/2_2144/')
    for cate in cateList :
        print(cate.string, cate['href'])
        #display the print content imediately
        sys.stdout.flush()
        f.write('\n'+cate.string+'\n')
        content = getCateContent(BASE_URL+cate['href'])[0].text.replace('\n', '')
        pretty_content = content.encode('utf-8').replace(b'\xc2\xa0', b'')
        # print(content)
        f.write(pretty_content.decode('utf-8'))
    
    f.close()


